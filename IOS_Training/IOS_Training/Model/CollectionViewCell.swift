//
//  CollectionViewCell.swift
//  IOS_Training
//
//  Created by Intime on 06/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
}
