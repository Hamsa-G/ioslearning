//
//  FirebaseModel.swift
//  IOS_Training
//
//  Created by Intime on 11/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import Foundation
public class FetchData {
    // MARK: Variables
    var name: String?
    var DOB: String?
    var email: String?
    var location: String?
    var profileImageURL: String?
    
    init(name: String, DOB: String, email: String, location: String, profile: String) {
        self.name = name
        self.DOB = DOB
        self.email = email
        self.location = location
        self.profileImageURL = profile
    }
}
