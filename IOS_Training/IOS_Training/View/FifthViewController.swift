//
//  FifthViewController.swift
//  IOS_Training
//
//  Created by Intime on 06/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FifthViewController: UIViewController, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager?

    // MARK: - Outlets
    @IBOutlet var myMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
    }
    
    // MARK: - Actions
    @IBAction func handleLocation(_ sender: UIButton) {
        self.getLocation()
    }
    
    func getLocation(){
        let placemark = CLLocationCoordinate2DMake(12.9715987, 77.59456)
        let anno = MKPointAnnotation()
        anno.coordinate = placemark
        anno.title = "current"
        
        let span = MKCoordinateSpan(latitudeDelta: 0.75, longitudeDelta: 0.75)
        let region = MKCoordinateRegion(center: anno.coordinate, span: span)
        self.myMap.setRegion(region, animated: true)
        self.myMap.addAnnotation(anno)
        self.myMap.selectAnnotation(anno, animated: true)
    }
}
