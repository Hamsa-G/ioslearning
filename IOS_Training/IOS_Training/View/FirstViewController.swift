//
//  FirstViewController.swift
//  IOS_Training
//
//  Created by Intime on 06/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
import Firebase

class FirstViewController: UIViewController, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    // MARK: Variables
    var data = [FetchData]()
    var images = [UIImage]()
    var videos = [String]()
    var status = [String]()
    var statusImage = [UIImage]()
    var titles = [String]()
    var views = [NSNumber]()
    // MARK: Outlets
    @IBOutlet var myCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        myCollectionView.register(FeedCollectionViewCell.self, forCellWithReuseIdentifier: "feedCell")
        let reference = Database.database().reference().child("Users")
        reference.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for users in snapshot.children.allObjects as! [DataSnapshot] {
                    let user = users.value as? [String: Any]
                    let userName = user?["name"] as! String
                    let userDOB = user?["DOB"] as! String
                    let userEmail = user?["Email"] as! String
                    let userLocation = user?["Location"] as! String
                    let userProfile = user?["imagePath"] as! String
                    self.data.append(FetchData(name: userName, DOB: userDOB, email: userEmail, location: userLocation, profile: userProfile))
                    print(self.data)
                }
              self.myCollectionView.reloadData()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        let url = NSURL(string: "https://s3-us-west-2.amazonaws.com/youtubeassets/home.json")
        URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                for dictionary in json as! [[String: AnyObject]] {
                    let title = dictionary["title"] as? String
                    let thumbnailImageName = dictionary["thumbnail_image_name"] as? String
                    let reviews = dictionary["number_of_views"] as? NSNumber
                    self.videos.append(thumbnailImageName!)
                    self.titles.append(title!)
                    self.views.append(reviews!)
                    
                    let channelDictionary = dictionary["channel"] as! [String: AnyObject]
                    let profileImageName = channelDictionary["profile_image_name"] as? String
                    self.status.append(profileImageName!)
                }
                DispatchQueue.main.async {
                    self.getImages()
                }
            } catch let jsonError {
                print(jsonError)
            }
            }.resume()
    }
    
    func getImages() {
        for i in 0..<videos.count {
            let urlString = videos[i]
            let url = NSURL(string: urlString)
            URLSession.shared.dataTask(with: url! as URL, completionHandler: { (data, respones, error) in
                
                if error != nil {
                    print(error!)
                    return
                }
                DispatchQueue.main.async {
                    if let imageData = data {
                        self.images.append(UIImage(data: imageData)!)
                        self.myCollectionView.reloadData()
                    }
                }
            }).resume()
        }
        
        for j in 0..<status.count {
            let urlString = videos[j]
            let url = NSURL(string: urlString)
            URLSession.shared.dataTask(with: url! as URL, completionHandler: { (data, respones, error) in
                
                if error != nil {
                    print(error!)
                    return
                }
                DispatchQueue.main.async {
                    if let imageData = data {
                        self.statusImage.append(UIImage(data: imageData)!)
                        self.myCollectionView.reloadData()
                    }
                }
            }).resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "feedCell", for: indexPath) as! FeedCollectionViewCell
        cell.post = data[indexPath.item]
        cell.profileImageView.image = images[indexPath.row]
        cell.statusImageView.image = statusImage[indexPath.row]
        cell.statusTextView.text = titles[indexPath.row]
        cell.likesCommentsLabel.text = "400 Likes   \(views[indexPath.row]) ReViews"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 400)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        myCollectionView.collectionViewLayout.invalidateLayout()
    }

}
