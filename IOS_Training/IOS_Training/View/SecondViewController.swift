//
//  SecondViewController.swift
//  IOS_Training
//
//  Created by Intime on 06/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
   // MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let searchText = self.textField.text
        if segue.identifier == "submit" {
            let controller = segue.destination as! MyCollectionViewController
            controller.query = searchText
        }
    }
}

