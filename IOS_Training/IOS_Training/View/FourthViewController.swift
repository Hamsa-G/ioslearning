//
//  FourthViewController.swift
//  IOS_Training
//
//  Created by Intime on 08/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class FourthViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    // MARK: Variables
    var picker: UIImagePickerController?
    var path: String?
    
    // MARK: outlets
    @IBOutlet var backGround: UIImageView!
    @IBOutlet var name: UITextField!
    @IBOutlet var DOB: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var location: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().barTintColor = UIColor(red: 59/255.0, green: 89/255.0, blue: 152/255.0, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        name.delegate = self
        DOB.delegate = self
        email.delegate = self
        location.delegate = self
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpUI() {
        picker = UIImagePickerController()
        picker?.delegate = self
        backGround.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGestureEvent(gesture:))))
        backGround.isUserInteractionEnabled = true
    }
    @objc func tapGestureEvent(gesture: UIGestureRecognizer) {
        let alert: UIAlertController = UIAlertController(title: "Profile pic options", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let gallaryAction = UIAlertAction(title: "Open Gallary", style: .default) { _ in self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in self.cancel()
        }
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallary() {
        picker?.allowsEditing = true
        picker?.sourceType = .photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    func cancel() {
        print(" cancel clicked")
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let fileManager = FileManager.default
        let documentsPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        let imagePath = documentsPath?.appendingPathComponent("image.png")
        self.path = imagePath?.absoluteString
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            try! UIImagePNGRepresentation(chosenImage)?.write(to: imagePath!)
            backGround.image = chosenImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            try! UIImagePNGRepresentation(originalImage)?.write(to: imagePath!)
            backGround.image = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Helper Methods
    func cleanCode() {
        self.name.text = ""
        self.DOB.text = ""
        self.email.text = ""
        self.location.text = ""
    }
    func alert(customTitle: String, customMessage: String) {
        let alert = UIAlertController(
            title: "" + customTitle,
            message: "" + customMessage,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: Validating Functions
    
    func validateUserName(str: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[0-9a-zA-Z\\_]{7,18}$", options: .caseInsensitive)
            if regex.matches(in: str, options: [], range: NSMakeRange(0, str.count)).count > 0 { return true}
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    func validateDOB(str : String) -> Bool {
        do {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-mm-yyyy"
            guard let _ = dateFormatter.date(from: str) else {
                return false
            }
        }
        return true
    }
    func validateEmail(str: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: str, options: [], range: NSRange(location: 0, length: str.count)) != nil
    }
    func validateUserLocation(str: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]{2,18}$", options: .caseInsensitive)
            if regex.matches(in: str, options: [], range: NSMakeRange(0, str.count)).count > 0 { return true}
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        name.resignFirstResponder()
        DOB.resignFirstResponder()
        email.resignFirstResponder()
        location.resignFirstResponder()
        return true
    }
  
    // MARK: - Action

    @IBAction func handleSavingProfile(_ sender: UIBarButtonItem) {
        if validateUserName(str: name.text!) && validateDOB(str: DOB.text!) && validateEmail(str: email.text!) && validateUserLocation(str: location.text!){
            let reference = Database.database().reference()
            let storageReference = Storage.storage().reference().child("myImage.png")
            let userReference = reference.child("Users")
            let key = userReference.childByAutoId().key
            if let uploadData = UIImagePNGRepresentation(backGround.image!) {
                storageReference.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    else {
                        print(metadata!)
                    }
                })
            }
            let user = [
                "id": key,
                "name": name.text!,
                "DOB": DOB.text!,
                "Email": email.text!,
                "Location": location.text!,
                "imagePath": path!
                ] as [String: Any]
            userReference.child(key).setValue(user)
            self.alert(customTitle: "Successful", customMessage: "Registered successfully!!")
            self.cleanCode()
        } else {
            self.alert(customTitle: "UnSuccessful", customMessage: "Either name, DOB or Email format is not matching!!")
            self.cleanCode()
        }
    }
    
    @IBAction func handleCancelAction(_ sender: UIBarButtonItem) {
        if name.text != "" || DOB.text != "" || email.text != "" || location.text != "" {
            let alert = UIAlertController(title: "Cancel?", message: "Do you really want to cancel?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (_) in
                self.cleanCode()
            }))
            alert.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
