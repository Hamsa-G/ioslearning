//
//  MyCollectionViewController.swift
//  IOS_Training
//
//  Created by Intime on 06/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit

class MyCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Variables
    var query: String?
    var urlArray: [String] = []
    var images: [UIImage] = []
    var imageNames: [String] = ["image 1","image 2","image 3","image 4","image 5","image 6","image 7","image 8","image 9","image 10"]
    var session: URLSession {
        let defaultConfig = URLSessionConfiguration.default
        defaultConfig.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        let session = URLSession(configuration: defaultConfig, delegate: nil, delegateQueue: OperationQueue.main)
        
        return session
    }
    
    @IBOutlet var myCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myCollectionView.delegate = self
        self.myCollectionView.dataSource = self
        
        let layout = self.myCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.myCollectionView.frame.size.width - 20)/2, height: (self.myCollectionView.frame.size.height)/3)
        parseJsonData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! CollectionViewCell
        cell.imageView.image = images[indexPath.row]
        cell.label.text = imageNames[indexPath.row]
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.lightGray.cgColor
        cell?.layer.borderWidth = 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.gray.cgColor
        cell?.layer.borderWidth = 2
    }
    
    // MARK: Helper Methods
    
    func parseJsonData() {
        let url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyCvdwWbJYFixDVnUp9dh70ocD3jlFWTfA4&cx=002153498475537447622:wp6-p8pjfxc&q=\(query!))"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) in
            if (error != nil)
            {
                print("error")
            }
            else {
                do {
                    let fetchdata = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! [String: Any]
                    let item = fetchdata["items"] as! [Any]
                    for i in 0..<item.count {
                        let item1 : [String: Any] = item[i] as! [String : Any]
                        let pagemap: [String:Any] = item1["pagemap"] as! [String : Any]
                        let imagesrc:[[String:Any]] = (pagemap["cse_image"] as? [[String:Any]])!
                        print("the pagemap is \(String(describing: imagesrc[0]["src"]))")
                        self.urlArray.append(imagesrc[0]["src"] as! String)
                    }
                    DispatchQueue.main.async {
                        self.getImages()
                    }
                    
                }
                catch {
                    print("error")
                }
            }
        }
        task.resume()
    }
    
    func getImages() {
        for i in 0..<urlArray.count {
            let url = urlArray[i]
            print(url)
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request) { (data, response, error) in
                if (error != nil)
                {
                    print("error")
                }
                DispatchQueue.main.async {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        self.images.append(image!)
                        self.myCollectionView.reloadData()
                    }
                    else {
                        print("Image is nill")
                    }
                }
            }
            task.resume()
        }
    }
}
