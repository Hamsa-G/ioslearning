//
//  AppDelegate.swift
//  IOSProgApp
//
//  Created by Intime on 16/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var baseController : UITabBarController?
    
    func createTabs(){
        
        var tabcontrollers : [UIViewController] = []
        for i in 0..<4{
            var tabController : UIViewController!
            var subController : UINavigationController!
            switch i{
                
            case 0:
                let layout = UICollectionViewFlowLayout()
                layout.minimumInteritemSpacing = 5
                layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
                layout.itemSize = CGSize(width: 350, height: 450)
                tabController = NewsFeedViewController(collectionViewLayout: layout)
                subController = UINavigationController(rootViewController: tabController)
                subController.tabBarItem.image = UIImage(named: "icon-notification")
                subController.navigationItem.title = "Icon"
                
            case 1:
                tabController = GoogleSearchViewController()
                subController = UINavigationController(rootViewController: tabController!)
                subController.navigationBar.barTintColor = UIColor(red: 51/255, green: 90/255, blue: 149/255, alpha: 1)
                subController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
                subController.navigationItem.title = "Want to Search some Image?"
                tabController.tabBarItem.image = UIImage(named: "Search")
                
            case 2:
                tabController = ProfileCreationViewController()
                subController = UINavigationController(rootViewController: tabController!)
                subController.navigationItem.title = "Profile"
                tabController.tabBarItem.image = UIImage(named: "profile")
                
            case 3:
                tabController = MapViewController()
                subController = UINavigationController(rootViewController: tabController!)
                subController.navigationItem.title = "Location"
                tabController.tabBarItem.image = UIImage(named: "location")
                
            default:
                tabController = nil
                
            }
            tabcontrollers.append(subController)
        }
        
        baseController = UITabBarController()
        baseController!.viewControllers = tabcontrollers
        
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        createTabs()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = baseController
        
        UITabBar.appearance().tintColor = UIColor.init(red: 70/255, green: 146/255, blue: 250/255, alpha: 1.0)
        
        application.statusBarStyle = .lightContent
       
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

