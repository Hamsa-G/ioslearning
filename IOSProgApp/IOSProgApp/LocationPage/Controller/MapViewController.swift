//
//  MapViewController.swift
//  IOSProgApp
//
//  Created by Intime on 16/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate {
    //MARK: Variables
    var locationManager: CLLocationManager?
    var searchButton: UIButton?
    var myMap: MKMapView?
    var locationProvider = UserLocationProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hue: 249/360, saturation: 100/100, brightness: 69/100, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationItem.title = "Location"

        setButton()
        setMapView()
        
        self.view.addSubview(searchButton!)
        self.view.addSubview(myMap!)
        
        searchButton?.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setButton() -> Void {
        searchButton = UIButton(frame: CGRect(x: 30, y: 100, width: 325, height: 30))
        searchButton?.setTitle("Get my location", for: .normal)
        searchButton?.backgroundColor = UIColor(hue: 15/360, saturation: 100/100, brightness: 80/100, alpha: 1.0)
        searchButton?.layer.cornerRadius = 15.0
        searchButton?.setTitleColor(UIColor.white, for: .normal)
    }
    
    func setMapView() {
        myMap = MKMapView(frame: CGRect(x: 30, y: 180, width: 325, height: 300))
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
    }
    
    @objc func buttonClicked(_ sender: AnyObject?) {
        print("button Clicked")
        let placemark = locationProvider.getCoordinates()
        let anno = MKPointAnnotation()
        anno.coordinate = placemark
        anno.title = "current"
        
        let span = MKCoordinateSpan(latitudeDelta: 0.75, longitudeDelta: 0.75)
        let region = MKCoordinateRegion(center: anno.coordinate, span: span)
        self.myMap?.setRegion(region, animated: true)
        self.myMap?.addAnnotation(anno)
        self.myMap?.selectAnnotation(anno, animated: true)
        
    }
}
