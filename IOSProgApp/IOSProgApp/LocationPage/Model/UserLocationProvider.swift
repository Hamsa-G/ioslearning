//
//  UserLocationProvider.swift
//  IOSProgApp
//
//  Created by Intime on 19/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
import MapKit

class UserLocationProvider: NSObject {
    func getCoordinates() -> (CLLocationCoordinate2D) {
        return CLLocationCoordinate2DMake(12.9175066, 77.6197909)
    }
}
