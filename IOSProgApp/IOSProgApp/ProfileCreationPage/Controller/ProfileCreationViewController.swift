//
//  ProfileCreationViewController.swift
//  IOSProgApp
//
//  Created by Intime on 16/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//
    import UIKit
    import CoreData
    
    class ProfileCreationViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        
        var person : [Person]!
        var profilePic : UIImageView?
        var firstNameTitleLabel : UILabel?
        var lastNameTitleLabel : UILabel?
        var firstNameTextView : UITextView?
        var lastNameTextView : UITextView?
        var saveButton : UIButton?
        var editButton : UIButton?
        var picker: UIImagePickerController?
        
        let uniqueId : Int = 1
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.view.backgroundColor = UIColor(red: 193/255, green: 96/255, blue: 0/255, alpha: 1.0)
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            navigationItem.title = "Profile Page"
        }
        
        override func viewWillAppear(_ animated: Bool) {
            fetchDataFromDb()
            loadPage()
        }
        
        override func viewDidDisappear(_ animated: Bool) {
            optimiseUI()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        private func loadPage(){
            setupProfilePic()
            setupNameLabels()
            setupNameTextViews()
            setupButtons()
        }
        
        private func optimiseUI(){
            person = nil
            profilePic?.removeFromSuperview()
            profilePic = nil
            firstNameTitleLabel?.removeFromSuperview()
            firstNameTitleLabel = nil
            lastNameTitleLabel?.removeFromSuperview()
            lastNameTitleLabel = nil
            firstNameTextView?.removeFromSuperview()
            firstNameTextView = nil
            lastNameTextView?.removeFromSuperview()
            lastNameTextView = nil
            saveButton?.removeFromSuperview()
            saveButton = nil
            editButton?.removeFromSuperview()
            editButton = nil
        }
        
        private func setupProfilePic() {
            profilePic = UIImageView(frame: CGRect(x: 10, y: 70, width: 60, height: 60))
            profilePic?.layer.borderWidth = 5.0
            profilePic?.layer.masksToBounds = true
            view.addSubview(profilePic!)
        }
        
        private func setupNameLabels(){
            firstNameTitleLabel = UILabel(frame: CGRect(x: 10, y: 150, width: 150, height: 40))
            firstNameTitleLabel?.text = "First Name :"
            firstNameTitleLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
            firstNameTitleLabel?.textColor = UIColor.black
            view.addSubview(firstNameTitleLabel!)
            
            lastNameTitleLabel = UILabel(frame: CGRect(x: 10, y: 200, width: 150, height: 40))
            lastNameTitleLabel?.text = "Last Name :"
            lastNameTitleLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
            lastNameTitleLabel?.textColor = UIColor.black
            view.addSubview(lastNameTitleLabel!)
        }
        
        private func setupNameTextViews(){
            firstNameTextView = UITextView(frame: CGRect(x: 120, y: 150, width: 200, height: 40))
            firstNameTextView?.font = UIFont.boldSystemFont(ofSize: 20.0)
            firstNameTextView?.layer.borderWidth = 1.2
            firstNameTextView?.layer.cornerRadius = 5.0
            firstNameTextView?.text = person[0].firstName
            firstNameTextView?.isEditable = false
            view.addSubview(firstNameTextView!)
            
            lastNameTextView = UITextView(frame: CGRect(x: 120, y: 200, width: 200, height: 40))
            lastNameTextView?.font = UIFont.boldSystemFont(ofSize: 20.0)
            lastNameTextView?.layer.borderWidth = 1.2
            lastNameTextView?.layer.cornerRadius = 5.0
            lastNameTextView?.text = person[0].lastName
            lastNameTextView?.isEditable = false
            view.addSubview(lastNameTextView!)
        }
        
        private func setupButtons(){
            editButton = UIButton(frame: CGRect(x: 50, y: 250, width: 100, height: 40))
            editButton?.setTitle("Edit", for: UIControlState.normal)
            editButton?.backgroundColor = UIColor.green
            editButton?.setTitleColor(UIColor.black, for: UIControlState.normal)
            editButton?.layer.borderWidth = 1.2
            editButton?.layer.cornerRadius = 5.0
            
            editButton?.addTarget(self, action: #selector(ProfileCreationViewController.editButtonClicked), for: UIControlEvents.touchUpInside)
            view.addSubview(editButton!)
            
            saveButton = UIButton(frame: CGRect(x: 180, y: 250, width: 100, height: 40))
            saveButton?.setTitle("Save", for: UIControlState.normal)
            saveButton?.backgroundColor = UIColor.blue
            saveButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            saveButton?.layer.borderWidth = 1.2
            saveButton?.layer.cornerRadius = 5.0
            saveButton?.isEnabled = false
            saveButton?.addTarget(self, action: #selector(ProfileCreationViewController.saveButtonClicked), for: UIControlEvents.touchUpInside)
            view.addSubview(saveButton!)
        }
        
        @objc private func editButtonClicked(){
            let alert = UIAlertController(title: "Edit??", message: "Do you really want to edit?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (_) in
                    self.enableTextViewAndButton(shouldEnable: true)
                }))
                alert.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
        
        @objc private func saveButtonClicked(){
            
            let isValidUI = validateUI()
            
            if isValidUI.0 {
                displayAlert(title: "Error!!!", message: isValidUI.1)
            } else {
                let context = managedObjectContext
                updateData(context: context!)
                displayAlert(title: "Success", message: "Successfully saved data")
            }
        }
        
        private func enableTextViewAndButton(shouldEnable: Bool){
            firstNameTextView?.isEditable = shouldEnable
            lastNameTextView?.isEditable = shouldEnable
            saveButton?.isEnabled = shouldEnable
            setUpCustomImage()
        }
        
        func setUpCustomImage() {
            picker = UIImagePickerController()
            picker?.delegate = self
            profilePic?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGestureEvent(gesture:))))
            profilePic?.isUserInteractionEnabled = true
        }
        @objc func tapGestureEvent(gesture: UIGestureRecognizer) {
            let alert: UIAlertController = UIAlertController(title: "Profile pic options", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            let gallaryAction = UIAlertAction(title: "Open Gallary", style: .default) { _ in self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in self.cancel()
            }
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        func openGallary() {
            picker?.allowsEditing = true
            picker?.sourceType = .photoLibrary
            present(picker!, animated: true, completion: nil)
        }
        
        func cancel() {
            print(" cancel clicked")
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
            if let chosenImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
                profilePic = UIImageView(frame: CGRect(x: 10, y: 70, width: 60, height: 60))
                profilePic?.image = chosenImage
                view.addSubview(profilePic!)
            } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
                profilePic = UIImageView(frame: CGRect(x: 10, y: 70, width: 60, height: 60))
                profilePic?.image = originalImage
                view.addSubview(profilePic!)
            }
            dismiss(animated: true, completion: nil)
        }
        
        // MARK: - Core Data stack
        
        lazy var applicationDocumentsDirectory: NSURL = {
            // The directory the application uses to store the Core Data store file. This code uses a directory named "com.sunil.Dummy" in the application's documents Application Support directory.
            let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            return urls[urls.count-1] as NSURL
        }()
        
        lazy var managedObjectModel: NSManagedObjectModel = {
            // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
            let modelURL = Bundle.main.url(forResource: "Person", withExtension: "momd")!
            return NSManagedObjectModel(contentsOf: modelURL)!
        }()
        
        lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
            // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
            // Create the coordinator and store
            var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
            let url = self.applicationDocumentsDirectory.appendingPathComponent("Person.sqlite")
            var error: NSError? = nil
            var failureReason = "There was an error creating or loading the application's saved data."
            do
            {
                try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
            }
            catch
            {
                print(failureReason)
            }
            return coordinator
        }()
        
        lazy var managedObjectContext: NSManagedObjectContext? = {
            // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
            let coordinator = self.persistentStoreCoordinator
            if coordinator == nil {
                return nil
            }
            var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            managedObjectContext.persistentStoreCoordinator = coordinator
            return managedObjectContext
        }()
        
        func fetchDataFromDb(){
            let context = managedObjectContext
            let request = NSFetchRequest<NSFetchRequestResult>()
            let entity = NSEntityDescription.entity(forEntityName: "Person", in: context!)
            request.entity = entity
            
            let predicate = NSPredicate(format: "id == %@", "\(uniqueId)")
            request.predicate = predicate
            
            do{
                person = try context!.fetch(request) as! [Person]
                print("\(person.count)")
                if person.count == 0 {
                    addNewPerson()
                }
            }catch{
                displayAlert(title: "Error!!!", message: "Failed to fetch data from DB")
            }
        }
        
        func addNewPerson(){
            print("Add new person")
            let context = managedObjectContext
            let newPerson = NSEntityDescription.insertNewObject(forEntityName: "Person", into: context!) as! Person
            newPerson.firstName = "Hamsa"
            newPerson.lastName = "Ganesh"
            newPerson.id = Int16(uniqueId)
            
            saveData(context: context!)
        }
        
        private func validateUI() -> (Bool,String){
            var errorMessage = ""
            var isError = false
            
            if firstNameTextView!.text!.isEmpty {
                errorMessage = "First Name cannot be blank"
                isError = true
            } else if lastNameTextView!.text!.isEmpty {
                errorMessage = "Last Name cannot be blank"
                isError = true
            }
            
            return (isError,errorMessage)
            
        }
        
        private func updateData(context: NSManagedObjectContext){
            person[0].firstName = firstNameTextView!.text!
            person[0].lastName = lastNameTextView!.text!
            saveData(context: context)
            enableTextViewAndButton(shouldEnable: false)
        }
        
        private func saveData(context: NSManagedObjectContext){
            do{
                try context.save()
                optimiseUI()
            }catch{
                displayAlert(title: "Error!!!", message: "Failed to save data")
            }
        }
        
        private func displayAlert(title: String, message: String){
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }



