//
//  FirebaseModel.swift
//  IOS_Training
//
//  Created by Intime on 11/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
class Post {
    // MARK: Variables
    var name: String?
    var profileImageName: String?
    var statusText: String?
    var statusImageName: String?
    var numLikes: Int?
    var numComments: Int?
    
    init(name: String, status: String, statusText: String, statusImage: String, likes: Int, comments: Int ) {
        self.name = name
        self.profileImageName = status
        self.statusText = statusText
        self.statusImageName = statusImage
        self.numLikes = likes
        self.numComments = comments
    }
}
    
