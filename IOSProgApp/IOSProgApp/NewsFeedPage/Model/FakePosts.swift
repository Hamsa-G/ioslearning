//
//  File.swift
//  IOSProgApp
//
//  Created by Intime on 17/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit
class FakePosts {
    func fetchPosts() -> [Post] {
        
        var posts = [Post]()
        
        let postMark = Post(name: "Mark Zuckerberg" , status: "zuckprofile", statusText: "By giving people the power to share, we're making the world more transparent.", statusImage: "zuckdog", likes: 400, comments: 123)
        posts.append(postMark)
        
        let postSteve = Post(name: "Steve Jobs", status: "steve_profile", statusText: "Design is not just what it looks like and feels like. Design is how it works.\n\n" +
            "Being the richest man in the cemetery doesn't matter to me. Going to bed at night saying we've done something wonderful, that's what matters to me.\n\n" +
            "Sometimes when you innovate, you make mistakes. It is best to admit them quickly, and get on with improving your other innovations.", statusImage: "steve_status", likes: 1000, comments: 55)
        posts.append(postSteve)
        
        let postGandhi = Post(name: "Mahatma Gandhi", status: "gandhi_profile", statusText: "Live as if you were to die tomorrow; learn as if you were to live forever.\n" +
            "The weak can never forgive. Forgiveness is the attribute of the strong.\n" +
            "Happiness is when what you think, what you say, and what you do are in harmony.", statusImage: "gandhi_status", likes: 1098, comments: 34)
        posts.append(postGandhi)
        
        let postZucker = Post(name: "Mark Zuckerburg", status: "zuckprofile", statusText: "Wise words from Will Smith: The only thing that I see that is distinctly different from me is: I'm not afraid to die on a treadmill. I will not be outworked, period. You might have more talent than me, you might be smarter than me, you might be sexier than me. You might be all of those things. You got it on me in nine categories. But if we get on the treadmill together, there's two things. You're getting off first, or I'm going to die. It's really that simple. Love that. I wish that you'll embrace this mindset in everything that you do. Tonight, when you think about 2017, don't set resolutions but set goals. Trust in your abilities to figure things out. No matter how small you start, start something that matters. With enough time, focus, effort, even resources and mentorship, you will develop new skills and achieve your goals.", statusImage: "1", likes: 345, comments: 234)
        posts.append(postZucker)
        
        return posts
    }
}
