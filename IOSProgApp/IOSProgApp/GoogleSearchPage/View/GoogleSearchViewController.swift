//
//  GoogleSearchViewController.swift
//  IOSProgApp
//
//  Created by Intime on 16/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit

class GoogleSearchViewController: UIViewController {
    var googleImageView: UIImageView?
    var searchLabel: UILabel?
    var searchField: UITextField?
    var searchButton: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor(red: 59/255, green: 170/255, blue: 0, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationItem.title = "Want to search some Image?"
        setImageView()
        setLabels()
        setTextFields()
        setButtons()

        self.view.addSubview(googleImageView!)
        self.view.addSubview(searchLabel!)
        self.view.addSubview(searchField!)
        self.view.addSubview(searchButton!)
        
        searchButton?.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
    }
    override func viewDidDisappear(_ animated: Bool) {
        optimiseUI()
    }
    func optimiseUI() {
        self.searchField?.text = nil
    }
    func setImageView() -> Void {
        googleImageView =  UIImageView(frame: CGRect(x: 67 , y: 69, width: 240, height: 70))
        let googleImage = UIImage(named: "GoogleLogo")
        googleImageView?.image = googleImage
    }
    func setLabels() -> Void {
        searchLabel = UILabel(frame: CGRect(x: 16, y: 171, width: 120, height: 30))
        searchLabel?.text = "Search"
        searchLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        searchLabel?.textColor = UIColor.blue
        
    }
    func setTextFields() -> Void {
        searchField = UITextField(frame: CGRect(x: 100, y: 167, width: 230, height: 30))
        searchField?.borderStyle = UITextBorderStyle.roundedRect
        searchField?.layer.cornerRadius = 8
        searchField?.layer.borderWidth = 1.5
        searchField?.placeholder = "Enter any text"
        searchField?.rightView = UIImageView(image: UIImage(named: "Search"))
        searchField?.rightViewMode = .unlessEditing
    }
    func setButtons() -> Void {
        searchButton = UIButton(frame: CGRect(x: 200, y: 215, width: 150, height: 30))
        searchButton?.setTitle("Search", for: .normal)
        searchButton?.backgroundColor = UIColor.blue
        searchButton?.layer.cornerRadius = 15.0
    }
    
    func ValidateUI() -> Bool {
        if (searchField?.text?.isEmpty)! {
            return false
        }
        return true
    }
    
    @objc func buttonClicked(_ sender: AnyObject?) {
        print("button Clicked")
        if ValidateUI() {
            if sender === searchButton {
                let searchText = searchField?.text
                let encounterViewController = MyCollectionViewController()
                encounterViewController.query = searchText
                self.navigationController?.pushViewController(encounterViewController, animated: true)
            }
        }
    }
}
