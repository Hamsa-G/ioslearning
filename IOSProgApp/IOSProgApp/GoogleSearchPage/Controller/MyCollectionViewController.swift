//
//  MyCollectionViewController.swift
//  IOS_Training
//
//  Created by Intime on 06/04/18.
//  Copyright © 2018 Intime. All rights reserved.
//

import UIKit

class MyCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Variables
    var query: String?
    var urlArray: [String] = []
    var images: [UIImage] = []
    var session: URLSession {
        let defaultConfig = URLSessionConfiguration.default
        defaultConfig.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        let session = URLSession(configuration: defaultConfig, delegate: nil, delegateQueue: OperationQueue.main)
        
        return session
    }
    
    
    var myCollectionView: UICollectionView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 51/255, green: 90/255, blue: 149/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationItem.title = "Want to Search some Image?"
        self.view.backgroundColor = UIColor(red: 59/255, green: 170/255, blue: 0, alpha: 1.0)
      
        parseJsonData()
        setCollectionView()
    }

    
    func setCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: (view.frame.width / 3) - 16, height: view.frame.height / 3)
        flowLayout.minimumInteritemSpacing = 2
        flowLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8)
        
        
        myCollectionView = UICollectionView(frame: CGRect(x: 10, y: 100, width: UIScreen.main.bounds.width-20, height: UIScreen.main.bounds.height-190), collectionViewLayout: flowLayout)
        
        myCollectionView?.contentMode = UIViewContentMode.scaleAspectFit
        myCollectionView?.backgroundColor = UIColor.magenta
        myCollectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "imageCell")
        
        myCollectionView?.delegate = self
        myCollectionView?.dataSource = self
        
        self.view.addSubview(myCollectionView!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
        let imageView : UIImageView = UIImageView(image: images[indexPath.row])
        imageView.layer.masksToBounds = true
        cell.contentMode = .scaleAspectFit
        cell.clipsToBounds = true
        cell.contentView.addSubview(imageView)
        return cell
    }
    
    // MARK: Helper Methods
    
    func parseJsonData() {
        let url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyCvdwWbJYFixDVnUp9dh70ocD3jlFWTfA4&cx=002153498475537447622:wp6-p8pjfxc&q=\(query!))"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) in
            if (error != nil)
            {
                print("error")
            }
            else {
                do {
                    let fetchdata = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! [String: Any]
                    let item = fetchdata["items"] as! [Any]
                    for i in 0..<item.count {
                        let item1 : [String: Any] = item[i] as! [String : Any]
                        let pagemap: [String:Any] = item1["pagemap"] as! [String : Any]
                        let imagesrc:[[String:Any]] = (pagemap["cse_thumbnail"] as? [[String:Any]])!
                        print("the pagemap is \(String(describing: imagesrc[0]["src"]))")
                        self.urlArray.append(imagesrc[0]["src"] as! String)
                    }
                    DispatchQueue.main.async {
                        self.getImages()
                    }
                }
                catch {
                    print("error")
                }
            }
        }
        task.resume()
    }
    
    func getImages() {
        for i in 0..<urlArray.count {
            let url = urlArray[i]
            print(url)
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request) { (data, response, error) in
                if (error != nil)
                {
                    print("error")
                }
                DispatchQueue.main.async {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        self.images.append(image!)
                        if self.images.count == self.urlArray.count {
                           self.myCollectionView?.reloadData()
                        }
                    }
                    else {
                        print("Image is nill")
                    }
                }
            }
            task.resume()
        }
    }
}
