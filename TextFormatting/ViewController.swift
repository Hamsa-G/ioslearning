//
//  ViewController.swift
//  TextFormatting
//
//  Created by sysadmin on 29/09/1939 Saka.
//  Copyright © 1939 sysadmin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var textField: VSTextField!
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.setFormatting("########-####-####-####-############", replacementChar: "#")
        textField.delegate = self
        button.isEnabled = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    @IBAction func acceptInput(_ sender: UIButton) {
        var modifiedInput = textField.text!
        
        if(modifiedInput.count == 32){
        let index = modifiedInput.index(modifiedInput.startIndex, offsetBy: 7)
        modifiedInput.insert("-", at: modifiedInput.index(after: index))
        
        let index1 = modifiedInput.index(modifiedInput.startIndex, offsetBy: 12)
        modifiedInput.insert("-", at: modifiedInput.index(after: index1))
        
        let index2 = modifiedInput.index(modifiedInput.startIndex, offsetBy: 17)
        modifiedInput.insert("-", at: modifiedInput.index(after: index2))
        
        let index3 = modifiedInput.index(modifiedInput.startIndex, offsetBy: 22)
        modifiedInput.insert("-", at: modifiedInput.index(after: index3))
        }
        
        let uuid = NSUUID(uuidString: modifiedInput)
        if(uuid == nil){
            let alert = UIAlertController(
                title: "Rejected",
                message: "Entered UUId is Invalid!!",
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
        let alert = UIAlertController(
            title: "Accepted",
            message: "Entered UUId is Valid!!",
            preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
    }
  
   
    @IBAction func textDidChanged(_ sender: VSTextField) {
        button.isEnabled = true
    }
    

}

